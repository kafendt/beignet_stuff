#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.hpp>
#endif

#define MAX_SOURCE_SIZE (0x100000)

int main(void) {
    // Create the two input vectors
    int i;
    const int LIST_SIZE = 1024;
    int *A = (int *)malloc(sizeof(int) * LIST_SIZE);
    int *B = (int *)malloc(sizeof(int) * LIST_SIZE);
    for (i = 0; i < LIST_SIZE; i++) {
        A[i] = i;
        B[i] = LIST_SIZE - i;
    }

    // Load the kernel source code into the array source_str
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen("gpu_kernels/vector_add_kernel.bin", "r");
    if (!fp) {
        fprintf(stderr, "Failed to load kernel.\n");
        exit(1);
    }
    source_str = (char *)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    // get all platforms (drivers), e.g. NVIDIA
    std::vector<cl::Platform> all_platforms;
    cl::Platform::get(&all_platforms);

    if (all_platforms.size()==0) {
        std::cout<<" No platforms found. Check OpenCL installation!\n";
        exit(1);
    }
    cl::Platform default_platform=all_platforms[0];
    std::cout << "Using platform: "<<default_platform.getInfo<CL_PLATFORM_NAME>()<<"\n";

    // get default device (CPUs, GPUs) of the default platform
    std::vector<cl::Device> all_devices;
    default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
    if(all_devices.size()==0){
        std::cout << " No devices found. Check OpenCL installation!\n";
        exit(1);
    }

    // use device[1] because that's a GPU; device[0] is the CPU
    cl::Device default_device=all_devices[0];
    for (auto& dev : all_devices)
    {
        std::cout << "Device found: " << dev.getInfo<CL_DEVICE_NAME>() << "\n";
    }
    std::cout << "Using device: " << default_device.getInfo<CL_DEVICE_NAME>() << "\n";

    // a context is like a "runtime link" to the device and platform;
    // i.e. communication is possible
    cl::Context context({default_device});

    // Create memory buffers on the device for each vector
    cl::Buffer a_buffer(context, CL_MEM_READ_ONLY, LIST_SIZE * sizeof(int));
    cl::Buffer b_buffer(context, CL_MEM_READ_ONLY, LIST_SIZE * sizeof(int));
    cl::Buffer c_buffer(context, CL_MEM_WRITE_ONLY, LIST_SIZE * sizeof(int));

    // Create a command queue
    cl_int err = CL_SUCCESS;
    cl::CommandQueue queue(context, default_device, 0, &err);

    // Copy the lists A and B to their respective memory buffers
    cl_int ret = queue.enqueueWriteBuffer(a_buffer, CL_TRUE, 0, LIST_SIZE * sizeof(int), A);
    ret = queue.enqueueWriteBuffer(b_buffer, CL_TRUE, 0, LIST_SIZE * sizeof(int), B);

    // Create a program from the kernel source
    // cl_program program = clCreateProgramWithSource(context, 1,
    //        (const char **)&source_str, (const size_t *)&source_size, &ret);
    cl_int binary_status;
    cl::Program::Binaries binary;
    binary.push_back(std::make_pair(source_str, source_size));

    // Build the program
    cl::Program program_ = cl::Program(context, all_devices, binary);
    program_.build(all_devices);

    // Create the OpenCL kernel
    cl::Kernel kernel(program_, "vector_add", &err);

    // Set the arguments of the kernel
    kernel.setArg(0, a_buffer);
    kernel.setArg(1, b_buffer);
    kernel.setArg(2, c_buffer);

    // Execute the OpenCL kernel on the list
    queue.enqueueNDRangeKernel(kernel, 0, cl::NDRange(LIST_SIZE));

    // Read the memory buffer C on the device to the local variable C
    int *C = (int *)malloc(sizeof(int) * LIST_SIZE);
    queue.enqueueReadBuffer(c_buffer, CL_TRUE, 0, LIST_SIZE * sizeof(int), C);

    // Display the result to the screen
    for (i = 0; i < LIST_SIZE; i++) printf("%d + %d = %d\n", A[i], B[i], C[i]);

    return 0;
}

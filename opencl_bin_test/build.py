#!/usr/bin/env python

import os

path = os.path.dirname(os.path.realpath(__file__))
build_path = "{}/build".format(path)

if not os.path.isdir(build_path):
    os.makedirs(build_path)

CTC_ROOTDIR="/home/nicolas/repos/hulks/cross_toolchains/ctc-linux64-hulks-8.2.0-1/"
os.chdir(build_path)
os.system("cmake -DCMAKE_TOOLCHAIN_FILE=" + CTC_ROOTDIR + "/toolchain.cmake ..")
os.system("make -j9")
os.rename("{}".format(path+"/build/compile_commands.json"),"{}".format(path+"/compile_commands.json"))
